###### Why not use the existing tools such as `OpenStudio PAT/Analysis Framework`, `JEPlus`, `Modelkit`, `Ladybug` and `Honeybee`?

None of them met my requirements, either for technical or arbitrary reasons:

- It does the job only partially
- Too much manual operations
- Unacquaintance with the programming language
- Too much computational resources are needed
- ...

Specifically:
- I tried `OpenStudio PAT` but I could not even launch a single simulation with it. I don't know what the exact reason was, but I ended up thinking that this is not meant to run on a laptop that I bought for € 340 in 2017. Also, I was already comfortable with Python and didn't want to learn the Ruby language (for writing `OpenStudio` measures).
- I have played a bit with `Ladybug` and `Honeybee`. I believe that it is in theory possible to do what I needed with those (especially if you define your own GhPython components), but I don't dare to think about what the canvases may look like...  I sincerely believe that I would have stopped before reaching my goal. Also, they are nice tools, but they can be quite frustrating for people who know how to program the usual way.

The main interest of `bstools` is to be written in Python, and to allow (after you properly configure/fix it, because it's still a prototype) to type one command, go to sleep and find all your graphs on the next day. It is suitable for an iterative approach, because everything is automated (also copying the results to sub-directories, auto-naming all the generated files etc. - like [here](https://gitlab.com/souliane/thesis/-/tree/master/plots)). If you realize that a setting was not correctly set, you can fix it and just relaunch the whole thing. The program will take several hours to run (well, it depends of your model and how many scenarios you simulate), but for you it is just a few seconds of work.

One major inconvenient is that it does not fit large-scale analysis, because of the poor performance (no parallelization / multi-threading). Also, the different scenarios have to be defined by yourself, there is no algorithm to dynamically generate scenarios based on previous results (you can easily generate thousands of scenarios with a few programming loops, but there is currently nothing to help you identity the best ones). If you need such features, look at `OpenStudio Analysis Framework` (OSAF), but it requires more than a cheap laptop.

###### How does it compare with `eppy`?

It uses `eppy` for manipulating the IDF file. So it does not compare at all, because it is based on it. Thanks to `eppy`, I was able to quickly implement higher level features, for example to dynamically define manual ventilation schedules for COVID-19 scenarios based on the classroom occupations. Or you can do things like "add mechanical ventilation devices and enable DCV in all the zones of that zone list" (in the end, similar to what you do with `OpenStudio` measures).

It also uses other Python libraries like `pandas`, `matplotlib`, `seaborn` and `psychrochart` to generate some graphs after running the simulations (including psychrometric chart of the unmet hours, where CO2 concentration levels are also displayed in addition to hygrometric values - c.f. [example](https://gitlab.com/souliane/thesis/-/blob/master/plots/energyplus/Jan-Dec/Natural/3a_D_W2y_psychro_unmet_hours.png) - that is a lot of information in a single graph).

`bstools` is basically about integrating a few Python libraries together with `energyplus`/`OpenStudio`, and to offer batch-processing, so that the full energy simulation workflow is automated: configuration, simulation, data post-processing (e.g. auto-detection of holidays), plotting of one or several scenarios on the same graph (which is convenient for comparing them).

###### How usable/stable is it?

Not much. See the TODO section in the README file.

###### Why is this hosted on Gitlab and not on Github?

Because Github is closed source.

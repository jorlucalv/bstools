#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd

from ..plotlib import Heatmap, Isolines, ODSProcessor, Room, ValueRange
from ..settings import Settings


class ODSLightingProcessor(ODSProcessor):
    settings = Settings.ODSLightingProcessor

    @staticmethod
    def get_title(sheet_name, room):
        # TODO: move these strings to settings.py
        return (
            sheet_name.replace("DF ", "")
            .replace("Lux ", "")
            .replace(room.key, room.name)
            .replace("Afternoon", "(05/15/2020 Afternoon Sunlight)")
            .replace("Night", "(Electric Lighting)")
        )

    def get_filepath(self, sheet_name) -> str:
        return str(self.output_dir / "{}_{{}}.png".format(sheet_name.replace(" ", "_")))

    def process_sheet(self, sheet_name, values):
        empty_column_index = self.get_first_empty_column_index(values)
        indoor_values = self.filter_values(values, end=empty_column_index)
        room = Room.get_by_key(self.settings.rooms, key=sheet_name.split(" ")[1])
        filepath = self.get_filepath(sheet_name)
        title = self.get_title(sheet_name, room)

        self.plot(
            indoor_values,
            "Illuminance [lux]",
            ValueRange(0, 2750, [0, 100, 150, 200, 300, 500, 1000, 2500]),
            filepath=filepath,
            room=room,
            title=title,
        )

        if sheet_name.endswith("Afternoon"):
            outdoor_values = self.filter_values(values, start=empty_column_index + 1)
            self.plot(
                values=self.percent_values(indoor_values, outdoor_values),
                filepath=filepath.replace("Lux", "DF"),
                title=title.replace("Lux", "Daylight Factors"),
                value_range=ValueRange(0, 30, [0, 2, 5, 10, 20, 30]),
                room=room,
                label="Daylight Factor [%]",
                fmt=".1f",
            )

    def plot(
        self, values, label, value_range: ValueRange, filepath, title, room, fmt=".0f"
    ):
        if room.lighting.rotation:
            values = room.lighting.rotation(values)

        df = pd.DataFrame(values)

        Heatmap(
            room.name, figure_size=room.figure_size, background=room.background
        ).plot(
            df,
            label=label,
            fmt=fmt,
            value_range=value_range,
            title=title,
            filepath=filepath,
        )

        Isolines(
            room.name,
            figure_size=room.figure_size,
            background=room.background,
            extrapolation=room.lighting.extrapolation,
        ).plot(
            df,
            value_range=value_range,
            title=title,
            filepath=filepath,
            decorate_graph_kwargs={"label": label, "fmt": fmt},
        )

# -*- coding: utf-8 -*-


class TooLessObjects(Exception):
    pass


class TooManyObjects(Exception):
    pass


def get(objs: list):
    if len(objs) == 1:
        return objs[0]
    elif not len(objs):
        raise TooLessObjects("Found no item instead of 1")
    else:
        raise TooManyObjects("Found {} items instead of 1".format(len(objs)))

# -*- coding: utf-8 -*-
import os
from collections import OrderedDict
from dataclasses import dataclass
from enum import Enum
from pathlib import Path

from .epluslib.data import DateRangeEnumMixin, Range, RepeatedRanges
from .plotlib import Dimension, Extrapolation, Lighting, Matrix, Room

project_path = Path(os.environ.get("PROJECTPATH", os.getcwd()))
input_path = project_path / "input"
output_path = project_path / "output"

Room.input_dir = input_path / "measurements/studied_rooms"


class Common:
    input_dir: Path = input_path / "measurements"
    output_dir: Path = output_path / "plots"
    rooms: tuple = (
        Room(
            "3a",
            "Classroom 3a",
            200.49,
            Dimension(500, 300),
            Dimension(0, 60),
            lighting=Lighting(Matrix.Rotation.counter_clockwise, Extrapolation.columns),
        ),
        Room(
            "3b",
            "Classroom 3b",
            199.59,
            Dimension(375, 440),
            Dimension(10, 15),
            lighting=Lighting(extrapolation=Extrapolation.rows),
        ),
        Room("3a_3b", "Classrooms 3a and 3b"),
        Room(
            "PauseHall",
            "Pause Hall",
            1301.04,
            Dimension(510, 280),
            Dimension(100, 110),
            lighting=Lighting(Matrix.Rotation.counter_clockwise, Extrapolation.columns),
        ),
        Room(
            "PhysicsRoom",
            "Physics Room",
            None,
            Dimension(436, 350),
            Dimension(40, 20),
            lighting=Lighting(Matrix.Rotation.clockwise),
        ),
        Room(
            "TeachersRoom",
            "Teachers' Room",
            301.98,
            Dimension(750, 280),
            Dimension(0, 90),
        ),
    )


@dataclass
class Quantity:
    name: str
    unit: str = None
    acceptable_range: dict = None

    # you can combine @classmethod and @property from Python 3.9
    @classmethod
    def carbon_dioxide(cls):
        return Quantity("CO2", "ppm", (None, 800))

    @classmethod
    def relative_humidity(cls):
        return Quantity("Relative Humidity", "%", (30, 70))

    @classmethod
    def temperature(cls):
        return Quantity("Temperature", "°C", (20, 28))

    @property
    def axis(self) -> str:
        """Returns the axis name for this quantity."""
        return "{} [{}]".format(self.name, self.unit) if self.unit else self.name

    @property
    def normalized_name(self) -> str:
        return self.name.lower().replace(" ", "_")


class Settings:
    class Workflow:
        openstudio: Path = Path("openstudio")
        base_path: Path = input_path / "OpenStudioModel"
        osw_path: Path = base_path / "Grein/workflow.osw"
        simulation_path: Path = base_path / "Grein/run"
        error_path: Path = simulation_path / "eplusout.err"
        ignored_error_path = base_path / "ignored_eplusout.err"
        results_path: Path = output_path / "results"

    class Energyplus:
        installation_path: Path = Path("/usr/local/openstudio-3.1.0/EnergyPlus")
        expand_objects_path = installation_path / Path("ExpandObjects")
        post_process_path = Path("/bstools/resources/EnergyPlus-9-4-0/PostProcess")
        hvac_diagram_path = post_process_path / "HVAC-Diagram"
        read_vars_eso_path = post_process_path / "ReadVarsESO"
        idf_path: Path = input_path / "OpenStudioModel/in.idf"
        osm_path: Path = input_path / "OpenStudioModel/Grein.osm"
        ventilated_space_types = ("Ventilated Rooms",)
        heated_space_types = (
            "Ventilated Rooms",
            "Special Classrooms",
            "Other Rooms",
            "Corridors",
            "Service Rooms",
        )

        class NaturalVentilation:
            class DateRange(DateRangeEnumMixin, Enum):
                summer = (("06/01", "09/30"),)
                winter = (("01/01", "05/31"), ("10/01", "12/31"))
                all_year = (("01/01", "12/31"),)

            schedule_name: str = "Natural Ventilation Availability Schedule"
            fixed_ranges = OrderedDict(
                (
                    # ventilate under occupation each hour for 15 minutes
                    (
                        "Mondays Tuesdays Wednesdays Thursdays",
                        RepeatedRanges(
                            on_hours=[Range("07:00", "12:00"), Range("13:00", "17:30")],
                            on_minutes=[Range("00", "15")],
                        ),
                    ),
                    (
                        "Fridays",
                        RepeatedRanges(
                            on_hours=[Range("07:00", "12:00"), Range("13:00", "16:00")],
                            on_minutes=[Range("00", "15")],
                        ),
                    ),
                )
            )

        class MechanicalVentilation:
            schedule_name = "Mechanical Ventilation Availability Schedule"
            night_cycle_schedule_name = "Night Cycle Applicability Schedule"

        class Heating:
            schedule_name = "Heating Availability Schedule"
            heating_setpoint_schedule_name = "School Heating Setpoint Schedule"
            cooling_setpoint_schedule_name = "School Cooling Setpoint Schedule"
            thermostat_name = "Thermostat Setpoint"

    class ReverberationTimeProcessor:
        input_dir: Path = Common.input_dir / "acoustics"
        output_dir: Path = Common.output_dir / "acoustics"
        rooms: tuple = Common.rooms
        zones: dict = {
            "3a": list(range(38, 48)) + list(range(60, 66)),
            "3b": range(5, 15),
            "PauseHall": range(16, 28),
            "TeachersRoom": range(28, 38),
        }
        # add this to the measured values to compensate for empty rooms (in seconds)
        optimal_reverberation_time_compensation_offset: float = 0.2

    class ODSLightingProcessor:
        input_dir: Path = Common.input_dir / "lighting"
        output_dir: Path = Common.output_dir / "lighting"
        rooms: tuple = Common.rooms
        filename: str = "20200515.ods"
        sheet_name_prefix: str = "Lux"

    class DatapointsProcessor:
        class DateRange(DateRangeEnumMixin, Enum):
            # From March 11th 2020, the school was closed
            year = ("2019-09-01", "2020-03-11")
            winter = ("2020-01-01", "2020-01-31")

        timezone_offset = 1  # results are for UTC and Vienna is UTC + 1
        input_dir: Path = Common.input_dir / "datapoints"
        output_dir: Path = Common.output_dir / "datapoints"
        rooms: tuple = Common.rooms
        room_key: str = "3a_3b"
        zones = {
            "3a": Common.input_dir / "datapoints/datapoints_3a.txt",
            "3b": Common.input_dir / "datapoints/datapoints_3b.txt",
        }
        description_filename: str = input_dir / "datapoints.txt"
        quantities: dict = {
            "cdi": Quantity.carbon_dioxide(),
            "windows": Quantity("Count Of Opened Windows"),
            "rhu": Quantity.relative_humidity(),
            "tem": Quantity.temperature(),
        }
        occupied_hours: tuple = ("07:00", "17:30")

        building_monitoring_system_url: str = NotImplemented

    class EnergyplusResultsProcessor:
        class DateRange(DateRangeEnumMixin, Enum):
            year = ("01-01", "12-31")
            summer = ("06-01", "06-30")
            winter = ("01-01", "01-31")

        @dataclass
        class Group:
            dirname: str
            variants: tuple = None
            date_ranges: list = None

        timezone_offset = 0  # simulation runs with local time already
        input_dir: Path = output_path / "results"
        output_dir: Path = Common.output_dir / "energyplus"
        rooms: tuple = Common.rooms
        room_key: str = "3a"
        zones = {"3a": "CLASSROOM 3A"}
        quantities: dict = {
            "cdi": Quantity.carbon_dioxide(),
            "rhu": Quantity.relative_humidity(),
            "tem": Quantity.temperature(),
        }
        simulation_year: int = 2020  # all energyplus simulations ran for 2020
        occupied_hours: tuple = ("07:00", "17:30")
        simulation_groups: tuple = (
            Group("AllScenarios"),
            Group(
                "MainScenarios",
                (
                    "D",
                    "DS_D6ren",
                    "DS_D10ren",
                    "DS_D14ren",
                    "DS_W2w_D6ren",
                    "DS_W2w_D10ren",
                    "DS_W2w_D14ren",
                    "D_W2y",
                    "D_W4y",
                ),
            ),
            Group("Natural", ("D", "D_W2y", "D_W4y")),
            Group(
                "Mechanical",
                (
                    "DS_D6ren",
                    "DS_D10ren",
                    "DS_D14ren",
                    "DS_W2w_D6ren",
                    "DS_W2w_D10ren",
                    "DS_W2w_D14ren",
                ),
                [DateRange.year.value, DateRange.winter.value],
            ),
            Group(
                "NoStrip",
                (
                    "D_W2w_D10ren",
                    "D_W2w_D14ren",
                    "DS_W2w_D10ren",
                    "DS_W2w_D14ren",
                ),
                [DateRange.winter.value],
            ),
            Group(
                "NoNight",
                (
                    "DS_W2w_D10re",
                    "DS_W2w_D14re",
                    "DS_W2w_D10ren",
                    "DS_W2w_D14ren",
                ),
            ),
            Group(
                "NoRecovery",
                (
                    "DS_W2w_D10en",
                    "DS_W2w_D14en",
                    "DS_W2w_D10ren",
                    "DS_W2w_D14ren",
                ),
                [DateRange.year.value],
            ),
            Group(
                "DCV",
                (
                    "DS_D10ren",
                    "DS_D14ren",
                    "DS_Dd800ren",
                    "DS_Dd1000ren",
                ),
            ),
            Group(
                "MechanicalSummer",
                (
                    "DS_D6ren",
                    "DS_D10ren",
                    "DS_D14ren",
                ),
                [DateRange.summer.value],
            ),
            Group(
                "RecoveryType",
                (
                    "DS_Dd800ren",
                    "DS_Dd800wen",
                ),
            ),
            Group(
                "COVID",
                (
                    "D_W4y_covid",
                    "D_Way_covid",
                    "D_Wa.50y_covid",
                    "D_Wa.25y_covid",
                ),
            ),
        )

    class OpenStudioResultsProcessor:
        input_dir: Path = output_path / "results"
        output_dir: Path = Common.output_dir / "openstudio"

    class WeatherFilesProcessor:
        input_dir: Path = input_path / "weather"
        output_dir: Path = output_path / "weather"

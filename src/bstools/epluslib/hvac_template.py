# -*- coding: utf-8 -*-
import json
import re
import sys
from difflib import unified_diff
from itertools import chain
from pathlib import Path

from pyexcel_io import save_data
from pyexcel_ods import save_data as save_data_to_ods

from ..lib.logger import step
from ..lib.os import OSTools
from ..settings import Settings
from .edit_idf import IDFEditor


class HVACTemplateExpander:

    idd_path = IDFEditor.idd_path
    expand_objects_path = Settings.Energyplus.expand_objects_path

    json_output = Path("HVACTemplate.json")
    csv_output = Path("HVACTemplate.csv")
    ods_output = Path("HVACTemplate.ods")

    dummy_value = "dummy"

    @classmethod
    def list_object_types(cls, idf_path: Path, excluded_types: list = None):
        with open(idf_path) as fp:
            content = fp.read()

        return sorted(
            set(
                [
                    obj_type
                    for obj_type in re.findall(r"\n([^\s]+),", content, re.DOTALL)
                    if obj_type not in (excluded_types or [])
                ]
            )
        )

    @classmethod
    def get_expansion_object_types(cls, idf_path: Path, expanded_idf_path: Path):
        return cls.list_object_types(
            expanded_idf_path,
            excluded_types=cls.list_object_types(idf_path)
            + ["Output:PreprocessorMessage"],
        )

    @classmethod
    def _build_expanded_idf_path(cls, idf_path: Path):
        return idf_path.parent / "expanded.idf"

    def _configure_for_expansion(self, parent_path: Path = None) -> Path:
        tmp_idd_path = (
            (parent_path / "Energy+.idd") if parent_path else Path("Energy+.idd")
        )
        if not tmp_idd_path.exists():
            OSTools.copy(self.idd_path, tmp_idd_path)
        return tmp_idd_path

    def expand_idf(self, idf_path: Path) -> Path:
        self._configure_for_expansion(idf_path.parent)
        OSTools.execute(
            str(self.expand_objects_path),
            shell=True,
            cwd=idf_path.parent,
        )
        return self._build_expanded_idf_path(idf_path)

    def get_results(self, overwrite: bool = False):
        if self.json_output.exists() and not overwrite:
            with open(self.json_output) as fp:
                return json.load(fp)
        else:
            results = {}

            parent_path = Path("tmp_hvac_template")
            if not parent_path.exists():
                OSTools.create_directory(parent_path)

            idf_path = parent_path / "in.idf"
            if idf_path.exists():
                idf_path.unlink()  # just to avoid problems after a previous run failed

            editor = IDFEditor(idf_path=idf_path)
            for template in editor.list_hvac_templates():
                if template in ("HVACTemplate:Plant:HotWaterLoop",):
                    step(f"Skipping {template}")
                else:
                    step(f"Processing {template}")
                    obj = editor.idf.newidfobject(template)
                    editor.set_attributes(
                        template,
                        {
                            attrname: self.dummy_value
                            for attrname in obj.objls[1:]
                            if not getattr(obj, attrname)
                        },
                    )
                    editor.save()
                    expanded_idf_path = self.expand_idf(idf_path)
                    results[
                        template.replace("HVACTemplate:", "")
                    ] = self.get_expansion_object_types(idf_path, expanded_idf_path)
                    editor.delete_objects(template)
                    editor.save()
                    expanded_idf_path.unlink()

            self.export_results(results)
            return results

    def export_results(self, results: dict):
        templates = sorted(results.keys())
        data = [
            ["Object"] + templates,
            *[
                [expanded_object]
                + [
                    "1" if expanded_object in results[template] else ""
                    for template in templates
                ]
                for expanded_object in sorted(set(chain(*results.values())))
            ],
        ]

        with open(self.json_output, "w") as fp:
            json.dump(results, fp)
        save_data(str(self.csv_output), data)
        save_data_to_ods(str(self.ods_output), {"HVACTemplate": data})
        print(
            f"Exported HVACTemplates to {self.json_output}, {self.csv_output} and {self.ods_output}"
        )

    def search(self, results: list, objects: set):
        invalid_objects = [
            obj for obj in objects if obj not in chain(*results.values())
        ]
        if invalid_objects:
            raise Exception(
                f"No HVACTemplate that expands to following objects: {invalid_objects}"
            )
        else:
            print(
                "HVACTemplates that expand to {}: {}".format(
                    sorted(objects),
                    [
                        template
                        for template in sorted(results.keys())
                        if objects.intersection(results[template]) == objects
                    ],
                )
            )

    def diff(self, results: list, templates: list):
        assert len(templates) == 2
        left, right = [re.sub("^HVACTemplate:", "", template) for template in templates]
        assert left in results.keys(), f"{left} is no HVACTemplate"
        assert right in results.keys(), f"{right} is no HVACTemplate"

        sys.stdout.writelines(
            [
                line
                for line in unified_diff(
                    sorted(f"{obj}\n" for obj in results[left]),
                    sorted(f"{obj}\n" for obj in results[right]),
                    fromfile=left,
                    tofile=right,
                    n=0,
                )
                if not line.startswith("@@")
            ]
        )

    def run(self, search_objects: str, diff_templates: str, overwrite: bool = False):
        results = self.get_results(overwrite)
        if search_objects:
            self.search(results, set(search_objects.split(",")))
        if diff_templates:
            self.diff(results, diff_templates.split(","))

# -*- coding: utf-8 -*-
from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Union

from ..lib.tools import get


class DateRangeEnumMixin:
    year = NotImplemented

    @classmethod
    def from_name(cls, name: str) -> Enum:
        return get([obj for obj in cls if obj.name == name])

    @classmethod
    def values(cls) -> list:
        return [obj.value for obj in cls]


@dataclass
class Range:
    start: str
    end: str


@dataclass
class RepeatedRanges:
    on_hours: List[Range]
    on_minutes: List[Range]


@dataclass
class DynamicRange:
    step: int
    duration: int


FixedRanges = Dict[str, List[RepeatedRanges]]
ScheduleRanges = Union[FixedRanges, DynamicRange]

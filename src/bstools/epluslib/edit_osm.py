# -*- coding: utf-8 -*-


import openstudio

from ..lib import logger
from ..lib.tools import get
from ..settings import Settings
from .edit_idf import Editor


class OSMEditor(Editor):
    osm = None

    @classmethod
    def get_path(cls):
        return openstudio.path(str(Settings.Energyplus.osm_path))

    @classmethod
    def get_attribute_index(cls, obj, attrname):
        return obj.iddObject().getFieldIndex(attrname).get()

    def load(self):
        self.osm = openstudio.model.Model.load(self.get_path()).get()

    def save(self, overwrite=False):
        if not self.osm.save(self.get_path(), overwrite):
            raise RuntimeError("Saving model did not succeed - try with overwrite=True")

    def get_zone_names(self, space_types: list):
        return sorted(
            "{} Thermal Zone".format(obj.name().get())
            for space_type in space_types
            for obj in self.get_objects(
                "OS:Space",
                "Space Type Name",
                space_type,
            )
        )

    def get_objects(
        self, obj_type: str, attrname: str = None, value: str = None
    ) -> list:
        return [
            obj
            for obj in self.osm.getObjectsByType(obj_type)
            if (attrname is None and value is None)
            or self.get_attribute(obj, attrname) == value
        ]

    def set_unique_object_attribute(self, obj_type: str, attrname: str, value: str):
        obj = get(self.osm.getObjectsByType(obj_type))
        return self.set_attribute(obj, attrname, value)

    def set_attribute(self, obj, attrname: str, value: str):
        obj.setString(self.get_attribute_index(obj, attrname), str(value))
        return self

    def get_attribute(self, obj, attrname: str):
        return obj.getString(self.get_attribute_index(obj, attrname)).get()


def osm_to_idf(osm_path: str, idf_path: str, overwrite: bool = False):
    logger.step(f"Converting {osm_path} to {idf_path}")
    if (
        not openstudio.energyplus.ForwardTranslator()
        .translateModel(
            openstudio.osversion.VersionTranslator()
            .loadModel(openstudio.path(osm_path))
            .get()
        )
        .save(openstudio.path(idf_path), overwrite)
    ):
        raise Exception(f"Failed to convert {osm_path} to {idf_path}")


def idf_to_osm(idf_path: str, osm_path: str, overwrite: bool = False):
    logger.step(f"Converting {idf_path} to {osm_path}")
    translator = openstudio.energyplus.ReverseTranslator()
    model = translator.translateWorkspace(
        openstudio.Workspace(openstudio.IdfFile.load(openstudio.path(idf_path)).get())
    )
    if model:
        types = sorted(
            set(o.iddObject().name() for o in translator.untranslatedIdfObjects())
        )
        if types:
            print("Untranslated IDF Objects:\n  {}".format("\n  ".join(types)))
        result = model.save(openstudio.path(osm_path), overwrite)
    else:
        result = False

    if not result:
        raise Exception(f"Failed to converting {idf_path} to {osm_path}")

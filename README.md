## Foreword

This software is in Pre-Alpha version. It is distributed for educational
purpose, and to show that it is possible to completely automate the `Energyplus`
and `OpenStudio` workflows with `Python`: programmatic configuration, batch
running, results post-processing, plotting graphs.

Actually, some features are not related to `Energyplus` or `OpenStudio` at all,
but they plot for example graphs from real measurements data. Just ignore them
if you are only interested in simulation.

## Development

### Scope

The current implementation strongly focuses on natural and mechanical
ventilation. It has been used to study the renovation of a school building,
more information here:
[Additional Refurbishment to a standard thermal retrofit of an educational building](https://repositum.tuwien.at/bitstream/20.500.12708/17810/1/Cossa%20Adrien%20-%202021%20-%20Additional%20Refurbishment%20to%20a%20Standard%20Thermal%20Retrofit%20of...pdf)
(pages 36�60).

It is a bit more than a proof of concept, but it remains a prototype: you can't
expect it to work out of the box for any other projects.

### TODO

Some implementation shortcuts have been taken and they remain as technical
debts - you will have to deal with them if you want to use this tool for
simulating with your own models.

Examples of open issues:
- Paths issues between `Windows`, `WSL` and `Linux`
- Make it work with an `IDF` only, without the need of using a  `OSM` file
- No more assumptions made about where to extract some information from the models
- Easier configuration of `settings.py` and `epluslib/variants.py`
- ...

See also page 105 of the previous PDF document.

## Installation

Since the software is experimental and you will probably need to fix a lot of
things on the go, I recommend to clone the repository, build or download the
`Docker` image (so that you don't have to mess up your environment with those
very tightly-pinned dependencies) and to work from within the container with
the project's directory mounted as a volume (so that you can modify files with
your favorite editor and automatically get the updates in the `Docker` container).

```
make docker_build  # or docker pull souliane/bstool:0.1
make docker_bash
```
The source code is copied to `/bstools` when building the image, and also
mounted there when the container starts.

If, for any reason, you can't work with `Docker`, it is also possible to use it
as a normal `Python` source code / package, but it won't be as easy.

## Running

The best way is currently to pass the environment variable `PROJECTPATH` to `run.py`:
```
PROJECTPATH=examples/Example_School src/bstools/run.py --help
```
If the package is installed, you can also call `run.py` from anywhere with
`bstools`.

### On Windows Subsystem for Linux (WSL)

Development has been initially done on `WSL 2` but using the `Energyplus` and
`Openstudio` binaries for Windows. There is now a `GNU/Linux` `Docker` image based on
`python:3.8.7`, in which `OpenStudio` (that also embeds `EnergyPlus`) has been
installed.

Just make sure to enable the `WSL` integration after installing `Docker` from
[here](https://www.docker.com/products/docker-desktop). If you experience
"Docker WSL integration unexpectedly stopped" issues, it might be that `Docker`
requires more memory... it could also be something else. Anyway, edit
`.wslconfig` in your `Windows` user's directory and give `WSL` at least 1GB memory:
```
[wsl2]
memory=1GB
```

## Testing

Because I don't have the time to write proper unit / functional / integration
tests, I currently just run some commands on the example data and compare the
output with so-called "golden files" - those are the output that I have already
carefully checked while developing. This solution temporary works, but it has
many disadvantages and should eventually be replaced or at least completed.

Tests can be run directly with `make tests` and also with `tox` (to create in
the background a dedicated environment, install the package and its dependency,
and then run the tests).

## Acknowledgements

Many thanks to Alexandra Heiderer who modeled the `Energyplus` `IDF` file for the
example school in `examples/Example_School`, and also for allowing me to
publicly share it here.

The people working on the `eppy` and the `openstudio` also made a great job,
thank you! And of course, to all the people who work on free software.
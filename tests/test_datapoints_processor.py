from pathlib import Path

import pytest

from bstools.plots.datapoints import DatapointsProcessor
from tests.test_time_series_processor import TimeSeriesProcessorTestMixin


class TestDatapointsProcessor(TimeSeriesProcessorTestMixin):
    processor_cls = DatapointsProcessor

    @pytest.fixture
    def feather_paths(self):
        return [
            Path(f"examples/Example_School/input/measurements/datapoints/{filename}")
            for filename in ("datapoints_3a.txt.feather", "datapoints_3b.txt.feather")
        ]

    def test_process_date_ranges(self):
        DatapointsProcessor.process_date_ranges(
            init_kwargs={"overwrite": True}, date_ranges=[self.date_range]
        )

        self.assert_similar_plots_directories(
            "examples/Example_School/output/plots/datapoints/Jan",
            "tests/assets/output/plots/datapoints/Jan",
        )

Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,61	 
125 Hz	1,11	 
160 Hz	0,99	 
200 Hz	1,28	 
250 Hz	1,03	 
315 Hz	1,47	 
400 Hz	1,60	 
500 Hz	1,46	 
630 Hz	1,55	 
800 Hz	1,56	 
1 kHz	1,64	 
1.25 kHz	1,56	 
1.6 kHz	1,61	 
2 kHz	1,48	 
2.5 kHz	1,39	 
3.15 kHz	1,21	 
4 kHz	1,05	 
5 kHz	0,96	 
6.3 kHz	0,87	 
8 kHz	0,72	 
10 kHz	0,56	 

T20 A	1,55	 
T20 Z	1,52	 

T30 A	1,68	 
T30 Z	1,52	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	N/A	 
125 Hz	1,02	?
160 Hz	0,95	 
200 Hz	1,26	 
250 Hz	1,29	 
315 Hz	1,51	 
400 Hz	1,56	 
500 Hz	1,66	 
630 Hz	1,74	 
800 Hz	1,64	 
1 kHz	1,71	 
1.25 kHz	1,72	 
1.6 kHz	1,73	 
2 kHz	1,59	 
2.5 kHz	1,45	 
3.15 kHz	1,28	 
4 kHz	1,11	 
5 kHz	1,04	 
6.3 kHz	0,90	 
8 kHz	0,74	 
10 kHz	0,59	 

T20 A	1,55	 
T20 Z	1,52	 

T30 A	1,68	 
T30 Z	1,52	 

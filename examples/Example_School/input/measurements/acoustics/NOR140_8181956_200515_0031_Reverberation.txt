Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	0,46	?
63 Hz	N/A	 
80 Hz	0,88	 
100 Hz	0,49	 
125 Hz	0,71	 
160 Hz	0,51	 
200 Hz	0,76	 
250 Hz	0,53	 
315 Hz	0,95	 
400 Hz	0,92	 
500 Hz	0,85	 
630 Hz	0,96	 
800 Hz	0,86	 
1 kHz	0,95	 
1.25 kHz	0,93	 
1.6 kHz	0,93	 
2 kHz	0,91	 
2.5 kHz	0,85	 
3.15 kHz	0,91	 
4 kHz	0,89	 
5 kHz	0,85	 
6.3 kHz	0,74	 
8 kHz	0,58	 
10 kHz	0,47	 

T20 A	0,91	 
T20 Z	0,91	 

T30 A	0,92	 
T30 Z	0,91	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,50	 
125 Hz	0,59	 
160 Hz	0,43	 
200 Hz	0,86	 
250 Hz	0,69	 
315 Hz	0,85	 
400 Hz	0,79	 
500 Hz	0,86	 
630 Hz	0,91	 
800 Hz	0,91	 
1 kHz	0,95	 
1.25 kHz	0,98	 
1.6 kHz	0,92	 
2 kHz	0,94	 
2.5 kHz	0,86	 
3.15 kHz	0,89	 
4 kHz	0,86	 
5 kHz	0,81	 
6.3 kHz	0,72	 
8 kHz	0,62	 
10 kHz	0,48	 

T20 A	0,91	 
T20 Z	0,91	 

T30 A	0,92	 
T30 Z	0,91	 

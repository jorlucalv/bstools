Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	2,68	 
80 Hz	1,32	 
100 Hz	1,27	 
125 Hz	1,47	 
160 Hz	1,46	 
200 Hz	1,62	 
250 Hz	1,46	 
315 Hz	1,60	 
400 Hz	1,72	 
500 Hz	1,61	 
630 Hz	1,83	 
800 Hz	1,76	 
1 kHz	1,57	 
1.25 kHz	1,91	 
1.6 kHz	1,81	 
2 kHz	1,68	 
2.5 kHz	1,44	 
3.15 kHz	1,38	 
4 kHz	1,25	 
5 kHz	1,12	 
6.3 kHz	0,95	 
8 kHz	0,78	 
10 kHz	0,64	 

T20 A	1,71	 
T20 Z	1,71	 

T30 A	1,76	 
T30 Z	1,71	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	2,16	 
100 Hz	1,94	 
125 Hz	1,86	 
160 Hz	1,63	 
200 Hz	1,50	 
250 Hz	1,41	 
315 Hz	1,68	 
400 Hz	1,75	 
500 Hz	1,78	 
630 Hz	1,82	 
800 Hz	1,80	 
1 kHz	1,65	 
1.25 kHz	1,85	 
1.6 kHz	1,76	 
2 kHz	1,67	 
2.5 kHz	1,47	 
3.15 kHz	1,35	 
4 kHz	1,25	 
5 kHz	1,14	 
6.3 kHz	0,99	 
8 kHz	0,80	 
10 kHz	0,64	 

T20 A	1,71	 
T20 Z	1,71	 

T30 A	1,76	 
T30 Z	1,71	 

Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	1,35	?
100 Hz	0,98	 
125 Hz	0,74	 
160 Hz	0,87	 
200 Hz	0,97	 
250 Hz	0,74	 
315 Hz	1,00	 
400 Hz	1,07	 
500 Hz	0,90	 
630 Hz	1,01	 
800 Hz	0,97	 
1 kHz	1,10	 
1.25 kHz	1,10	 
1.6 kHz	1,01	 
2 kHz	0,90	 
2.5 kHz	0,84	 
3.15 kHz	0,81	 
4 kHz	0,81	 
5 kHz	0,89	 
6.3 kHz	0,75	 
8 kHz	0,64	 
10 kHz	0,48	 

T20 A	0,90	 
T20 Z	0,84	 

T30 A	0,91	 
T30 Z	0,84	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	1,15	?
125 Hz	0,98	 
160 Hz	1,00	 
200 Hz	0,95	 
250 Hz	0,89	 
315 Hz	0,96	 
400 Hz	0,97	 
500 Hz	1,01	 
630 Hz	0,97	 
800 Hz	0,98	 
1 kHz	0,98	 
1.25 kHz	1,01	 
1.6 kHz	0,93	 
2 kHz	0,95	 
2.5 kHz	0,90	 
3.15 kHz	0,83	 
4 kHz	0,80	 
5 kHz	0,83	 
6.3 kHz	0,74	 
8 kHz	0,65	 
10 kHz	0,54	 

T20 A	0,90	 
T20 Z	0,84	 

T30 A	0,91	 
T30 Z	0,84	 

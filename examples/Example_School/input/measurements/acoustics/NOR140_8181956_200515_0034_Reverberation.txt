Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	5,42	 
63 Hz	N/A	 
80 Hz	0,64	 
100 Hz	0,27	?
125 Hz	0,50	 
160 Hz	0,71	 
200 Hz	0,67	 
250 Hz	0,65	 
315 Hz	0,84	 
400 Hz	0,93	 
500 Hz	0,78	 
630 Hz	0,92	 
800 Hz	0,85	 
1 kHz	0,81	 
1.25 kHz	0,77	 
1.6 kHz	0,86	 
2 kHz	0,86	 
2.5 kHz	0,91	 
3.15 kHz	0,94	 
4 kHz	0,88	 
5 kHz	0,81	 
6.3 kHz	0,70	 
8 kHz	0,63	 
10 kHz	0,45	 

T20 A	0,86	 
T20 Z	0,87	 

T30 A	0,85	 
T30 Z	0,87	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,51	?
125 Hz	0,56	 
160 Hz	0,78	 
200 Hz	0,80	 
250 Hz	0,65	 
315 Hz	0,86	 
400 Hz	0,89	 
500 Hz	0,76	 
630 Hz	0,86	 
800 Hz	0,82	 
1 kHz	0,83	 
1.25 kHz	0,86	 
1.6 kHz	0,88	 
2 kHz	0,86	 
2.5 kHz	0,92	 
3.15 kHz	0,91	 
4 kHz	0,86	 
5 kHz	0,83	 
6.3 kHz	0,72	 
8 kHz	0,61	 
10 kHz	0,47	 

T20 A	0,86	 
T20 Z	0,87	 

T30 A	0,85	 
T30 Z	0,87	 

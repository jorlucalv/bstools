FROM python:3.8.7

ARG OPENSTUDIO_DOWNLOAD_URL=https://github.com/NREL/OpenStudio/releases/download/v3.1.0/OpenStudio-3.1.0+e165090621-Linux.deb
RUN wget -nc $OPENSTUDIO_DOWNLOAD_URL
RUN dpkg -i `basename $OPENSTUDIO_DOWNLOAD_URL`

WORKDIR /bstools

COPY . .

RUN pip install -r requirements.txt
RUN pip install -r requirements-dev.txt
RUN pip install -e .

CMD [ "/bin/bash" ]